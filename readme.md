**Simple test example**   
You can run it from IDE or from console.   
**./gradlew clean test** - execute tests  
**./gradlew allureAggregateServe** - open allure report 

In **config.properties** you can setup global site url for all tests and selenium hub url if we need remote.  
**logback.xml** - log system configuration appenders/rollingPolicy/root log level 
**ChromeTests.xml/FirefoxTests.xml** - testNG config for tests 