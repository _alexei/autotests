package Pages;
public class LoginPage {
    public static final String usernameInput = "//input[@name='username']";
    public static final String passwordsInput = "//input[@name='password']";
    public static final String signInButton = "//div[@class='sign-in__btns']/button";

    public static final String dashBoardLogo = "//img[@class='logo-text']";
    public static final String dashboardButton = "(//li[@class='left-menu-item'])[2]";
    public static final String workflowsButton = "(//li[@class='left-menu-item'])[7]";

    public static final String menuCaption = "//div[@class='header-page-title']";
    public static final String createButton = "//button[@class='a-button primary']";
    public static final String timelineTabButton = "//a[@href='#tab1']";

    public static final String addReminderButton = "(//li[contains(@class,'add-reminder')])[1]";
    public static final String addCallButton = "(//li[contains(@class,'call')])[1]";
    public static final String addEmailButton = "(//li[contains(@class,'email')])[1]";
    public static final String saveTemplateButton = "//a[@class='save js-save-template-btn']";
    public static final String workflowTitle = "//h3[@class='workflow-title']";
}
