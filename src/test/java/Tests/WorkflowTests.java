package Tests;

import Utils.AllureUtils;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Slf4j
@Epic("Workflows")
@Feature("Create workflow")
public class WorkflowTests extends BaseTest {
    String currentUrl,workflowName;

    @Test(priority = 1)
    @Description("New workflow with actions Add Call to Customer; Send Email for current timeline")
    @Parameters({"email","pass"})
    public void WorkflowTest1(String email, String pass) throws InterruptedException {
        Selenide.open(siteUrl);
        $(new By.ByXPath("//input[@name='username']")).sendKeys(email);
        $(new By.ByXPath("//input[@name='password']")).sendKeys(pass);
        AllureUtils.saveScreen("Login form filled");
        log.info("Login form filled");
        $(new By.ByXPath("//div[@class='sign-in__btns']/button")).click();

        //checking login
        $(new By.ByXPath("//img[@class='logo-text']")).should(Condition.exist);
        AllureUtils.saveScreen("Main page loaded");
        log.info("Main page loaded");

        //go to Workflows
        $(new By.ByXPath("(//li[@class='left-menu-item'])[7]")).click();
        $(new By.ByXPath("//div[@class='header-page-title']")).should(Condition.exactText("Workflows"));
        AllureUtils.saveScreen("Workflows loaded");
        log.info("Workflows loaded");

        //create new workflow
        $(new By.ByXPath("//button[@class='a-button primary']")).click();
        $(new By.ByXPath("//a[@href='#tab1']")).should(Condition.exist);
        AllureUtils.saveScreen("Set up workflow");
        log.info("Set up workflow");

        //TODO: Make some research sometimes tests fails here
        //go to timeline tab and add actions
        $(new By.ByXPath("//a[@href='#tab1']")).click();
        $(new By.ByXPath("(//li[contains(@class,'add-reminder')])[1]")).hover();
        TimeUnit.MILLISECONDS.sleep(200);
        $(new By.ByXPath("(//li[contains(@class,'call')])[1]")).click();
        $(new By.ByXPath("//a[@class='save js-save-template-btn']")).click();
        TimeUnit.MILLISECONDS.sleep(200);

        $(new By.ByXPath("(//li[contains(@class,'add-reminder')])[1]")).hover();
        TimeUnit.MILLISECONDS.sleep(200);
        $(new By.ByXPath("(//li[contains(@class,'email')])[1]")).click();
        $(new By.ByXPath("//a[@class='save js-save-template-btn']")).click();
        AllureUtils.saveScreen("Actions added");
        log.info("Actions added");

        //save workflow name & url
        currentUrl = WebDriverRunner.getWebDriver().getCurrentUrl();
        workflowName = $(new By.ByXPath("//h3[@class='name']")).getText();

        //return to dashboard and find our flow
        $(new By.ByXPath("(//li[@class='left-menu-item'])[2]")).click();
        $(new By.ByXPath("(//li[@class='left-menu-item'])[7]")).click();
        $$(new By.ByXPath("//h3[@class='workflow-title']")).filter(Condition.exactText(workflowName)).first().click();
        TimeUnit.MILLISECONDS.sleep(100);
        Assert.assertEquals(currentUrl,WebDriverRunner.getWebDriver().getCurrentUrl());
        AllureUtils.saveScreen("Workflow saved");
        $(new By.ByXPath("//img[@class='logo-text']")).should(Condition.exist);
        log.info("Workflow saved");

        TimeUnit.SECONDS.sleep(5);
        Selenide.closeWindow();
    }

}
