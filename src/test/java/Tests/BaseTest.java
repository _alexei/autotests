package Tests;

import Utils.AllureUtils;
import com.codeborne.selenide.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public class BaseTest {
     WebDriver driver;  //current WebDriver instance
     String browserType = "chrome"; //The value is taken from the xml configuration or the default - chrome
     String siteUrl; // The value is taken from config.properties file

     /**
      *  It will execute before test starts and set all params for test
      *  Also selenide configured heer
      */
     @BeforeClass
     public void beforeClass(ITestContext context) {
           try (InputStream input = new FileInputStream("config.properties")) {
                //get site url from config.properties
                Properties prop = new Properties();
                prop.load(input);
                siteUrl = prop.getProperty("siteUrl");

                //set selenium remote property from config.properties if env variable is present
                if( System.getProperty("useRemoteSeleniumGrid")  != null ){
                   Configuration.remote = prop.getProperty("seleniumHubUrl");
                }
           } catch (IOException ex) {
                ex.printStackTrace();
           }

           //checking if test runs manually standalone
           if(context.getCurrentXmlTest().getParameter("browser") != null){
                browserType = context.getCurrentXmlTest().getParameter("browser");
           }

          Configuration.browserSize = "1920x1080";
           Configuration.browserPosition = "0x0";
          Configuration.timeout = 10000; //in ms how long wait before fail test
          if(browserType.equals("chrome")){ Configuration.browser = "chrome"; }
          if(browserType.equals("firefox")){ Configuration.browser = "firefox"; }
     }

     /**
      * It will execute after every test and take screenshot if test fails
      * @param result
      */
     @AfterMethod
     public void tearDown(ITestResult result)
     {
          if(ITestResult.FAILURE==result.getStatus())
          {
               AllureUtils.saveScreen("Test fail screenshot");
          }
     }


}
