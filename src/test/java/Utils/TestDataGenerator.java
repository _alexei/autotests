package Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class TestDataGenerator {

    //arrays for generating names
    private static String[] nameArr = {"Liam", "Noah","Oliver","Elijah","James","Olivia",
           	                              "Emma","Ava","Charlotte","Sophia"};
    private static String[] lastNameArr = {"Smith", "Johnson", "Williams", "Brown", "Jones",
                                            "Garcia", "Miller", "Davis" };

    /**
     * Generates email with current dateTime timestamp
     * @return string email
     */
    public static String generateEmail(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
        return dateFormat.format(new Date()) + "@gmail.com";
    }

    /**
     * Generates Random username
     * @return string username
     */
    public static String generateName(){
        Random r = new Random();
        return  nameArr[r.nextInt(nameArr.length)]
                + generateAlphabeticalString(8)
                + " "
                + nameArr[r.nextInt(lastNameArr.length)]
                + generateAlphabeticalString(8);
    }

    /**
     * Generates random number from 1 to maxInt
     * @return int
     */
    public static int generateNumber(){
        return new Random().nextInt(Integer.MAX_VALUE);
    }

    /**
     * Generates random Alphanumeric string
     * @param len length of string
     * @return string
     */
    public static String generateAlphanumericString(int len){
        return generateASCII(len,48, 122 );
    }
    /**
     * Generates random Alphabetical string
     * @param len length of string
     * @return string
     */
    public static String generateAlphabeticalString(int len){
        return generateASCII(len,97, 122 );
    }

    /**
     * Generates string contain different symbols
     * @param len length of string
     * @return string
     */
    public static String generateSymbolsString(int len){
        return generateASCII(len,32, 125 );
    }

    /**
     * Generates string contain almost all that you can type
     * @param length length of string
     * @param left start symbol from ASCII
     * @param right start symbol from ASCII
     * @return string
     */
    public static String generateASCII(int length, int left, int right){
        Random random = new Random();

        String generatedString = random.ints(left, right + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }

}
