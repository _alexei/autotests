package Utils;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Allure;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;

import java.io.ByteArrayInputStream;

@Slf4j
public class AllureUtils {
    /**
     * Method saves screenshot to allure report and capture possible errors
     * @param message
     */
    public static void saveScreen(String message){
        try{
            Allure.addAttachment(message, new ByteArrayInputStream(Selenide.screenshot(OutputType.BYTES)));
        }catch (Exception e){
            log.error("Exception while taking screenshot "+e.getMessage());
        }
    }
}
